package br.unisc;

import java.util.LinkedList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@SessionScoped
@ManagedBean
public class HammingGerador {

	private String metotoConversor;
	private String mensagemConversao;
	private String mensagemConvertida;
	private String tipoParidade;
	private String mensagemEnviada;
	private String linhaAlterada;
	private String posicaoAlterada;
	private List<DadosResposta> listaMensagens;
	private List<DadosResposta> listaMensagensDeteccao;

	public HammingGerador() {
		listaMensagens = new LinkedList<>();
		listaMensagensDeteccao = new LinkedList<>();
		posicaoAlterada = "";
		linhaAlterada = "";
	}

	private String[] mensagemRecebida = new String[12];
	private static final String ASCII_PARA_BINARIO = "0";
	private static final String BINARIO_PARA_ASCII = "1";

	public void executaConversao() {
		mensagemConvertida = null;
		if (metotoConversor.equals(ASCII_PARA_BINARIO)) {
			mensagemConvertida = stringParaBinario(mensagemConversao);
		}
		if (metotoConversor.equals(BINARIO_PARA_ASCII)) {
			mensagemConvertida = binarioParaString(mensagemConversao);
		}
	}
	
	//QUESTÃO 1
	public static String stringParaBinario(String s) {
		byte[] bytes = s.getBytes();
		StringBuilder binary = new StringBuilder();
		for (byte b : bytes) {
			int val = b;
			for (int i = 0; i < 8; i++) {
				binary.append((val & 128) == 0 ? 0 : 1);
				val <<= 1;
			}
			binary.append(' ');
		}
		return binary.toString();
	}
	
	//QUESTÃO 2
	public void executaHamming() {

		String mensagemEnviadaBinario = stringParaBinario(mensagemEnviada);

		listaMensagens.clear();
		listaMensagensDeteccao.clear();

		String[] str = null;

		str = mensagemEnviadaBinario.split(" ");

		int linha = 1;
		for (String st : str) {
			String[] resultado = calculaHamming(st, linha);

			int count = 0;
			boolean ok = true;
			for (String r : resultado) {
				if (!resultado[count].equals(mensagemRecebida[count])) {
					ok = false;
				}
				count++;
			}
			linha++;

		}
	}
	
	//QUESTÃO 3
	public static String binarioParaString(String str) {

		String[] temp;
		String delimiter = " ";
		temp = str.split(delimiter);
		String ch = "";
		for (int i = 0; i < temp.length; i++) {
			int c = Integer.parseInt(temp[i], 2);
			ch += Character.toString((char) c).charAt(0);
		}
		return ch;

	}

	//QUESTÃO 4
	public void chamaHamming1() {
		executaHamming();
	}

	public String[] calculaHamming(String st, int linha) {

		String[] msg = new String[8];

		for (int l = 0; l < 8; l++) {
			msg[l] = st.substring(l, l + 1);
		}

		mensagemRecebida[2] = msg[0];
		mensagemRecebida[4] = msg[1];
		mensagemRecebida[5] = msg[2];
		mensagemRecebida[6] = msg[3];
		mensagemRecebida[8] = msg[4];
		mensagemRecebida[9] = msg[5];
		mensagemRecebida[10] = msg[6];
		mensagemRecebida[11] = msg[7];

		String[] gerado = new String[12];
		String[] P1 = new String[5];
		String[] P2 = new String[5];
		String[] P4 = new String[4];
		String[] P8 = new String[4];

		Integer resultadoP1 = 0;
		Integer resultadoP2 = 0;
		Integer resultadoP4 = 0;
		Integer resultadoP8 = 0;

		gerado[0] = "?";
		gerado[1] = "?";
		gerado[3] = "?";
		gerado[7] = "?";

		int countI = 0;
		int count = 0;
		for (String i : gerado) {
			if (i == null) {
				gerado[count] = msg[countI];
				countI++;
			}
			count++;
		}

		P1 = getP1(gerado);
		int c = 0;
		for (String i : P1) {
			if (c > 0) {
				resultadoP1 += new Integer(i);
			}
			c++;
		}

		resultadoP1 = resultadoP1 % 2;
		mensagemRecebida[0] = resultadoP1.toString();

		P2 = getP2(gerado);
		c = 0;
		for (String i : P2) {
			if (c > 0) {
				resultadoP2 += new Integer(i);
			}
			c++;
		}
		resultadoP2 = resultadoP2 % 2;
		mensagemRecebida[1] = resultadoP2.toString();

		P4 = getP4(gerado);
		c = 0;
		for (String i : P4) {
			if (c > 0) {
				resultadoP4 += new Integer(i);
			}
			c++;
		}
		resultadoP4 = resultadoP4 % 2;
		mensagemRecebida[3] = resultadoP4.toString();

		P8 = getP8(gerado);
		c = 0;
		for (String i : P8) {
			if (c > 0) {
				resultadoP8 += new Integer(i);
			}
			c++;
		}
		resultadoP8 = resultadoP8 % 2;
		mensagemRecebida[7] = resultadoP8.toString();

		DadosResposta dados = new DadosResposta();
		dados.setAscEnviado(binarioParaString(st));
		dados.setBit1(resultadoP1.toString());
		dados.setBit2(resultadoP2.toString());
		dados.setBit3(mensagemRecebida[2]);
		dados.setBit4(resultadoP4.toString());
		dados.setBit5(mensagemRecebida[4]);
		dados.setBit6(mensagemRecebida[5]);
		dados.setBit7(mensagemRecebida[6]);
		dados.setBit8(resultadoP8.toString());
		dados.setBit9(mensagemRecebida[8]);
		dados.setBit10(mensagemRecebida[9]);
		dados.setBit11(mensagemRecebida[10]);
		dados.setBit12(mensagemRecebida[11]);
		dados.setAscRecebido(binarioParaString(dados.getBit3() + dados.getBit5() + dados.getBit6() + dados.getBit7()
				+ dados.getBit9() + dados.getBit10() + dados.getBit11() + dados.getBit12()));

		listaMensagens.add(dados);

		gerado[0] = resultadoP1.toString();
		gerado[1] = resultadoP2.toString();
		gerado[3] = resultadoP4.toString();
		gerado[7] = resultadoP8.toString();

		mensagemRecebida[0] = resultadoP1.toString();
		mensagemRecebida[1] = resultadoP2.toString();
		mensagemRecebida[3] = resultadoP4.toString();
		mensagemRecebida[7] = resultadoP8.toString();

		DadosResposta dadosResultado = new DadosResposta();
		dadosResultado.setAscEnviado(binarioParaString(st));

		if (posicaoAlterada.equals("1") && linhaAlterada != null && linha == new Integer(linhaAlterada)) {
			if (gerado[0] == "1") {
				dadosResultado.setBit1("0");
			} else {
				dadosResultado.setBit1("1");
			}
		} else {
			dadosResultado.setBit1(gerado[0]);
		}

		if (posicaoAlterada.equals("2") && linhaAlterada != null && linha == new Integer(linhaAlterada)) {
			if (gerado[1] == "1") {
				dadosResultado.setBit2("0");
			} else {
				dadosResultado.setBit2("1");
			}
		} else {
			dadosResultado.setBit2(gerado[1]);
		}
		if (posicaoAlterada.equals("3") && linhaAlterada != null && linha == new Integer(linhaAlterada)) {
			if (gerado[2].equals("1")) {
				dadosResultado.setBit3("0");
			} else {
				dadosResultado.setBit3("1");
			}
		} else {
			dadosResultado.setBit3(gerado[2]);
		}

		if (posicaoAlterada.equals("4") && linhaAlterada != null && linha == new Integer(linhaAlterada)) {
			if (gerado[3].equals("1")) {
				dadosResultado.setBit4("0");
			} else {
				dadosResultado.setBit4("1");
			}
		} else {
			dadosResultado.setBit4(gerado[3]);//
		}

		if (posicaoAlterada.equals("5") && linhaAlterada != null && linha == new Integer(linhaAlterada)) {
			if (gerado[4].equals("1")) {
				dadosResultado.setBit5("0");
			} else {
				dadosResultado.setBit5("1");
			}
		} else {
			dadosResultado.setBit5(gerado[4]);
		}

		if (posicaoAlterada.equals("6") && linhaAlterada != null && linha == new Integer(linhaAlterada)) {
			if (gerado[5].equals("1")) {
				dadosResultado.setBit6("0");
			} else {
				dadosResultado.setBit6("1");
			}
		} else {
			dadosResultado.setBit6(gerado[5]);
		}

		if (posicaoAlterada.equals("7") && linhaAlterada != null && linha == new Integer(linhaAlterada)) {
			if (gerado[6].equals("1")) {
				dadosResultado.setBit7("0");
			} else {
				dadosResultado.setBit7("1");
			}
		} else {
			dadosResultado.setBit7(gerado[6]);
		}

		if (posicaoAlterada.equals("8") && linhaAlterada != null && linha == new Integer(linhaAlterada)) {
			if (gerado[7].equals("1")) {
				dadosResultado.setBit8("0");
			} else {
				dadosResultado.setBit8("1");
			}
		} else {
			dadosResultado.setBit8(gerado[7]);
		}

		if (posicaoAlterada.equals("9") && linhaAlterada != null && linha == new Integer(linhaAlterada)) {
			if (gerado[8].equals("1")) {
				dadosResultado.setBit9("0");
			} else {
				dadosResultado.setBit9("1");
			}
		} else {
			dadosResultado.setBit9(gerado[8]);
		}

		if (posicaoAlterada.equals("10") && linhaAlterada != null && linha == new Integer(linhaAlterada)) {
			if (gerado[9].equals("1")) {
				dadosResultado.setBit10("0");
			} else {
				dadosResultado.setBit10("1");
			}
		} else {
			dadosResultado.setBit10(gerado[9]);
		}

		if (posicaoAlterada.equals("11") && linhaAlterada != null && linha == new Integer(linhaAlterada)) {
			if (gerado[10].equals("1")) {
				dadosResultado.setBit11("0");
			} else {
				dadosResultado.setBit11("1");
			}
		} else {
			dadosResultado.setBit11(gerado[10]);
		}

		if (posicaoAlterada.equals("12") && linhaAlterada != null && linha == new Integer(linhaAlterada)) {
			if (gerado[11].equals("1")) {
				dadosResultado.setBit11("0");
			} else {
				dadosResultado.setBit11("1");
			}
		} else {
			dadosResultado.setBit12(gerado[11]);
		}

		dadosResultado.setAscRecebido(binarioParaString(dadosResultado.getBit3() + dadosResultado.getBit5()
				+ dadosResultado.getBit6() + dadosResultado.getBit7() + dadosResultado.getBit9()
				+ dadosResultado.getBit10() + dadosResultado.getBit11() + dadosResultado.getBit12()));

		dadosResultado.setResultado("Ok!");
		if (!dados.getBit1().equals(dadosResultado.getBit1())) {
			dadosResultado.setResultado("Erro no bit 1");
		}
		if (!dados.getBit2().equals(dadosResultado.getBit2())) {
			dadosResultado.setResultado("Erro no bit 2");
		}
		if (!dados.getBit3().equals(dadosResultado.getBit3())) {
			dadosResultado.setResultado("Erro no bit 3");
		}
		if (!dados.getBit4().equals(dadosResultado.getBit4())) {
			dadosResultado.setResultado("Erro no bit 4");
		}
		if (!dados.getBit5().equals(dadosResultado.getBit5())) {
			dadosResultado.setResultado("Erro no bit 5");
		}
		if (!dados.getBit6().equals(dadosResultado.getBit6())) {
			dadosResultado.setResultado("Erro no bit 6");
		}
		if (!dados.getBit7().equals(dadosResultado.getBit7())) {
			dadosResultado.setResultado("Erro no bit 7");
		}
		if (!dados.getBit8().equals(dadosResultado.getBit8())) {
			dadosResultado.setResultado("Erro no bit 8");
		}
		if (!dados.getBit9().equals(dadosResultado.getBit9())) {
			dadosResultado.setResultado("Erro no bit 9");
		}
		if (!dados.getBit10().equals(dadosResultado.getBit10())) {
			dadosResultado.setResultado("Erro no bit 10");
		}
		if (!dados.getBit11().equals(dadosResultado.getBit11())) {
			dadosResultado.setResultado("Erro no bit 11");
		}
		if (!dados.getBit12().equals(dadosResultado.getBit12())) {
			dadosResultado.setResultado("Erro no bit 12");
		}
		listaMensagensDeteccao.add(dadosResultado);

		return gerado;

	}
	//QUESTÃO 5
	public void chamaHamming2() {
		executaHamming();
	}

	//QUESTÃO 8
		public void executaConversaoBitParidade() {
			mensagemConvertida = null;
			if (metotoConversor.equals(ASCII_PARA_BINARIO)) {
				mensagemConvertida = stringParaBinarioParidade(mensagemConversao);
			}
		}
		
		public String stringParaBinarioParidade(String s) {
			byte[] bytes = s.getBytes();
			StringBuilder binary = new StringBuilder();
			for (byte b : bytes) {
				int val = b;
				for (int i = 0; i < 8; i++) {
					binary.append((val & 128) == 0 ? 0 : 1);
					val <<= 1;
				}
				
				int total = 0;
				for (int l = 0; l < 8; l++) {
					total += new Integer( binary.substring(l, l + 1) );
				}
				if( tipoParidade.equals("IMPAR") ) {
					if( ( total % 2 ) == 0 ) {
						binary.append( "1" );
					} else {
						binary.append( "0" );
					}
				} else {
					binary.append(total % 2);
				}
				
				binary.append(' ');
			}
			return binary.toString();
		}
	
	private static String[] getP1(String[] gerado) {
		String[] resultado = new String[6];
		int count = 0;
		int countP1 = 0;

		for (String i : gerado) {
			if ((count % 2) == 0 && countP1 <= 5) {
				resultado[countP1] = i;
				countP1++;
			}
			count++;
		}

		return resultado;
	}

	private static String[] getP2(String[] gerado) {

		String[] resultado = new String[6];
		int count = 0;
		int countP2 = 0;
		for (String i : gerado) {
			if (count == 1) {
				resultado[countP2] = i;
				countP2++;
			}
			if (count == 2) {
				resultado[countP2] = i;
				countP2++;
			}
			if (count == 5) {
				resultado[countP2] = i;
				countP2++;
			}
			if (count == 6) {
				resultado[countP2] = i;
				countP2++;
			}
			if (count == 9) {
				resultado[countP2] = i;
				countP2++;
			}

			if (count == 10) {
				resultado[countP2] = i;
				countP2++;
			}
			count++;
		}

		return resultado;
	}

	private static String[] getP4(String[] gerado) {

		String[] resultado = new String[5];
		int count = 0;
		int countP4 = 0;
		for (String i : gerado) {
			if (count == 3) {
				resultado[countP4] = i;
				countP4++;
			}
			if (count == 4) {
				resultado[countP4] = i;
				countP4++;
			}
			if (count == 5) {
				resultado[countP4] = i;
				countP4++;
			}
			if (count == 6) {
				resultado[countP4] = i;
				countP4++;
			}
			if (count == 11) {
				resultado[countP4] = i;
				countP4++;
			}
			count++;
		}

		return resultado;
	}

	private static String[] getP8(String[] gerado) {
		String[] resultado = new String[5];
		int count = 0;
		int countP8 = 0;

		for (String i : gerado) {
			if (count >= 7) {
				resultado[countP8] = i;
				countP8++;
			}
			count++;
		}

		return resultado;
	}

	public String getMensagemEnviada() {
		return mensagemEnviada;
	}

	public void setMensagemEnviada(String mensagemEnviada) {
		this.mensagemEnviada = mensagemEnviada;
	}

	public String[] getMensagemRecebida() {
		return mensagemRecebida;
	}

	public void setMensagemRecebida(String[] mensagemRecebida) {
		this.mensagemRecebida = mensagemRecebida;
	}

	public String getMetotoConversor() {
		return metotoConversor;
	}

	public void setMetotoConversor(String metotoConversor) {
		this.metotoConversor = metotoConversor;
	}

	public String getMensagemConversao() {
		return mensagemConversao;
	}

	public void setMensagemConversao(String mensagemConversao) {
		this.mensagemConversao = mensagemConversao;
	}

	public String getMensagemConvertida() {
		return mensagemConvertida;
	}

	public void setMensagemConvertida(String mensagemConvertida) {
		this.mensagemConvertida = mensagemConvertida;
	}

	public List<DadosResposta> getListaMensagens() {
		return listaMensagens;
	}

	public void setListaMensagens(List<DadosResposta> listaMensagens) {
		this.listaMensagens = listaMensagens;
	}

	public List<DadosResposta> getListaMensagensDeteccao() {
		return listaMensagensDeteccao;
	}

	public void setListaMensagensDeteccao(List<DadosResposta> listaMensagensDeteccao) {
		this.listaMensagensDeteccao = listaMensagensDeteccao;
	}

	public String getLinhaAlterada() {
		return linhaAlterada;
	}

	public void setLinhaAlterada(String linhaAlterada) {
		this.linhaAlterada = linhaAlterada;
	}

	public String getPosicaoAlterada() {
		return posicaoAlterada;
	}

	public void setPosicaoAlterada(String posicaoAlterada) {
		this.posicaoAlterada = posicaoAlterada;
	}

	public String getTipoParidade() {
		return tipoParidade;
	}

	public void setTipoParidade(String tipoParidade) {
		this.tipoParidade = tipoParidade;
	}

}
