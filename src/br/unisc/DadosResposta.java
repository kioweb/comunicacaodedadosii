package br.unisc;

public class DadosResposta {

	private String ascEnviado;
	private String bit1;
	private String bit2;
	private String bit3;
	private String bit4;
	private String bit5;
	private String bit6;
	private String bit7;
	private String bit8;
	private String bit9;
	private String bit10;
	private String bit11;
	private String bit12;
	private String ascRecebido;
	private String resultado;

	public String getBit1() {
		return bit1;
	}

	public void setBit1(String bit1) {
		this.bit1 = bit1;
	}

	public String getBit2() {
		return bit2;
	}

	public void setBit2(String bit2) {
		this.bit2 = bit2;
	}

	public String getBit3() {
		return bit3;
	}

	public void setBit3(String bit3) {
		this.bit3 = bit3;
	}

	public String getBit4() {
		return bit4;
	}

	public void setBit4(String bit4) {
		this.bit4 = bit4;
	}

	public String getBit5() {
		return bit5;
	}

	public void setBit5(String bit5) {
		this.bit5 = bit5;
	}

	public String getBit6() {
		return bit6;
	}

	public void setBit6(String bit6) {
		this.bit6 = bit6;
	}

	public String getBit7() {
		return bit7;
	}

	public void setBit7(String bit7) {
		this.bit7 = bit7;
	}

	public String getBit8() {
		return bit8;
	}

	public void setBit8(String bit8) {
		this.bit8 = bit8;
	}

	public String getBit9() {
		return bit9;
	}

	public void setBit9(String bit9) {
		this.bit9 = bit9;
	}

	public String getBit10() {
		return bit10;
	}

	public void setBit10(String bit10) {
		this.bit10 = bit10;
	}

	public String getBit11() {
		return bit11;
	}

	public void setBit11(String bit11) {
		this.bit11 = bit11;
	}

	public String getAscEnviado() {
		return ascEnviado;
	}

	public void setAscEnviado(String ascEnviado) {
		this.ascEnviado = ascEnviado;
	}

	public String getAscRecebido() {
		return ascRecebido;
	}

	public void setAscRecebido(String ascRecebido) {
		this.ascRecebido = ascRecebido;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public String getBit12() {
		return bit12;
	}

	public void setBit12(String bit12) {
		this.bit12 = bit12;
	}

}
