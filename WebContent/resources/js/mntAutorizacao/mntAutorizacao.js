'use strict';

/* Controller da manutenção de Unimed's */

var autorizacaoApp = angular.module( 'autorizacaoApp', [] );

autorizacaoApp.controller( 'listaAutorizacoes', function( $scope, $http ) {

	$http.get( '/Auditoria/getDados.jsp?pg=mntAutorizacoes&cp=lista' ).success( function( data ) {
		$scope.lista = data;
	} );
} );
