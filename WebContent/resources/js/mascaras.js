$(document).ready(function() {

	$(".maskRG").mask("9999999999?99");
	$(".cdCpf").mask("999.999.999-99");
	$(".maskPlaca").mask("aaa-9999");
	$(".maskProposta").mask("9?99999");
	$(".maskCep").mask("99999-999");
	$(".maskTelefone").mask("(99)999999?99");
	$(".maskQtde").mask("9?99");
	$(".maskPortas").mask("9?9");
	$(".maskData").mask("99/99/9999");
	$(".maskMesAno").mask("99/9999");
	$(".maskEstado").mask("AA");
	$(".maskIbge").mask("9999999");
	$(".maskAno").mask("9999");
});

function moeda(z){
	v = z.value;
	v=v.replace(/\D/g,"") // permite digitar apenas numero
	v=v.replace(/(\d{1})(\d{1,2})$/,"$1.$2") // coloca virgula antes dos ultimos 2 digitos
	z.value = v;
}
