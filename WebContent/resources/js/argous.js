$(function(){
	argous = new Argous();
});

Argous.prototype.$modulos = new Object();

function Argous() {
	console.log( "inicializando o Argous..." );
};

Argous.prototype.registraModulo = function( nomeModulo, initFunc ) {
	
	console.log( "Registrando: " + nomeModulo );
	
	$objetos = new Object();
	this.$modulos[nomeModulo] = $objetos;
	
	initFunc( $objetos );
	
	this.inic( $objetos, nomeModulo );
};

Argous.prototype.inic = function( $objetos, nomeModulo ) {
	
	console.log( "inic " + nomeModulo );
	this.initTbls( $objetos, nomeModulo );
	this.initFlds( $objetos, nomeModulo );
};

Argous.prototype.initFlds = function( $objetos, nomeModulo ) {
	
	console.log( "initFlds: " + nomeModulo );
	var flds = document.getElementsByTagName( "input" );
	var dadosModelo = $objetos["model"];
	
	for( ipt in flds ) {

		if( !isNaN( ipt ) ) {
			
			var field = flds[ipt];
			var nomeCampo = field.getAttribute( "name" );
			var vlrCampo = field.getAttribute( "value" );
			
			if( vlrCampo != null && vlrCampo.charAt( 0 ) == "{" ) {
				var fonte = vlrCampo.substring( 1, vlrCampo.length - 1 );
				var tks = fonte.split( "." );
				
				if( tks.length == 2 ) {
					console.log( nomeCampo + " -> " + fonte + " - " + tks[ 0 ] + ":" + tks[ 1 ] );
					var dadosFonte = dadosModelo[tks[0]];
					var valorDado = dadosFonte[tks[1]];
					
					if( valorDado != null ) {
						field.value = valorDado;
						console.log( "valor do campo: " + valorDado );
					} else {
						console.log( "????????????????" );
					}
				}
			}
		}
	} 
};

Argous.prototype.initTbls = function( $objetos, nomeModulo ) {
	
	console.log( "initTbls: " + nomeModulo );
	var tbls = document.getElementsByTagName( "table" );

	for( tbl in tbls ) {
        if( !isNaN( tbl ) ) {
        	var tabela = tbls[tbl];
        	var nomeFonte = tabela.getAttribute("ag-tbl");
        	console.log( "nomeFonte --)>)> " + nomeFonte );
        	if( $objetos.hasOwnProperty( "model" ) ) {

        		console.log( "Encontrei model -> " + $objetos.hasOwnProperty( "model" ) );

        		var dadosFonte = $objetos["model"];
        		var dadosTbl = dadosFonte[nomeFonte];
        		
            	var linhas = tabela.getElementsByTagName("tr");
            	
            	for( linha in linhas ) {
            		
            		if( !isNaN( linha ) ) {
            			
            			var linhaBase = linhas[ linha ];
            			var base = linhaBase.getAttribute( "linhaBase" ); 
            			
            			if( base != null ) {
                    		this.carregaTabela( nomeModulo, tabela, linhaBase, dadosTbl );
            			}
            		}
            	}

        	} else {
            	console.log( "mas nao encontrei model " );
        	}
        }
	}
	
	console.log( "Finalizando..." );
};

Argous.prototype.sayHello = function( $objetos, nomeModulo ) {
	
	var tbls = document.getElementsByTagName( "table" );

	for( tbl in tbls ) {
        if( !isNaN( tbl ) ) {
        	var tabela = tbls[tbl];
        	var attr = tabela.getAttribute("ag-tbl");
        	
        	if( attr != null ) {
            	
            	var linhas = tabela.getElementsByTagName("tr");
            	
            	for( linha in linhas ) {
            		
            		if( !isNaN( linha ) ) {
            			
            			var linhaBase = linhas[ linha ];
            			var nomeFonte = linhaBase.getAttribute( attr ); 
            			
            			if( nomeFonte ) {
                        	if( $objetos.hasOwnProperty( nomeFonte ) ) {
                        		var fonte = $objetos[nomeFonte];
                        		this.carregaTabela( nomeModulo, tabela, linhaBase, fonte );
                        	}
            			}
            		}
            	}
        	}
        }
	}
	
//	alert( "Hello Lucio: " + this.$objetos.hasOwnProperty("variavel" ) ); 
//	
//	for( obj in this.$objetos ) {
//		alert( "Será --> " + obj + " : " + this.$objetos[obj] );
//	}
}; 

Argous.prototype.carregaTabela = function( nomeModulo, tb, l, tblData ) {
	
	console.log( "carregaTabela: " + tblData );
	carregandoOn();
	
	var t = document.getElementById( "tbl" ).tBodies[0];
    var dif = tb.rows.length - t.rows.length;
    
    t.deleteRow( l.rowIndex - dif );
    
	if( tblData.indexOf( "ajax(" ) == 0 ) {

		console.log( "chamando ajax" );
		this.callAjax(nomeModulo,t,l,tblData);
		return;
	}
    
	console.log( "atualizando..." );
	
	this.atualizaLinhasTabela( tblData, l, t );
	
	SelecionaPrimeiraLinha(1);
	
	carregandoOff();
	
};

Argous.prototype.atualizaLinhasTabela = function( tblData, l, t ) {
	
	var j = 1;
    for( ind in tblData ) {
    	
    	var ln = tblData[ ind ];
    	
        var c = l.cloneNode(true);
        c.setAttribute('onCLick','javascript:SelecionaLinhaClick(' + j + ')'); 
        c.id = "l" + j++;
        
        
        for( x in c.cells ) {
            
            var k = parseInt( x );
            
            if( !isNaN( k ) ) {
            	c.cells[ k ].innerHTML = ln[ k ];
            }
        }

        t.appendChild( c );
    }
};

Argous.prototype.callAjax = function(nomeModulo,t,l,fonteDados) {

	var end = "./" + nomeModulo + ".ajax";
	
	$.ajax( {
		type: "POST",
		url: end,
		data: fonteDados,
		dataType: "json",
		success: function( data ) {
			console.log( "<--recebi dados--> " + data );
			carregandoOff();
			argous.atualizaLinhasTabela( data, l, t);
		},
		error: function( xhr, ajaxOptions, thrownError ) {
			alert( "ERRO AJAX: " + xhr.responseText );
			carregandoOff();
		}
	} );	
};


function carregandoOn() {
   var lock = document.getElementById('carregando'); 
   if( lock ) {
	   lock.className = 'CarregandoOn'; 
	   lock.innerHTML = "<img src='../imgs/carregando.gif' /> <br /> Carregando..."; 
   };
};

function carregandoOff() {
	var lock = document.getElementById('carregando');
	lock.setAttribute('class', 'CarregandoOff' );
};