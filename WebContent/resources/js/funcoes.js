function aumentarObsAndamentos(prm){
	$('.' + prm ).attr('rows', '4');
}

function diminuirObsAndamentos(prm){
	$('.' + prm ).attr('rows', '1');
}

function carregaOn(prm) {

	document.getElementById("posload").style.display = "none";
	document.getElementById("preload").style.display = "block";

}

function carregaOff() {
	document.getElementById("posload").style.display = "block";
	document.getElementById("preload").style.display = "none";

}

$(function() {
	$(".tpData").datepicker();
});

function moeda(z) {
	v = z.value;
	v = v.replace(/\D/g, ""); // permite digitar apenas números
	v = v.replace(/[0-9]{12}/, "inválido"); // limita pra máximo 999.999.999,99
	v = v.replace(/(\d{1})(\d{1,2})$/, "$1.$2"); // coloca virgula antes dos
	// últimos 2 digitos
	z.value = v;
}

function altura(z) {
	v = z.value;
	v = v.replace(/\D/g, ""); // permite digitar apenas números
	v = v.replace(/[0-9]{5}/, "inválido"); // limita pra máximo 999.999.999,99
	v = v.replace(/(\d{1})(\d{1,1})$/, "$1.$2"); // coloca virgula antes dos
	// últimos 2 digitos
	z.value = v;
}

function peso(z) {
	v = z.value;
	v = v.replace(/\D/g, ""); // permite digitar apenas números
	v = v.replace(/[0-9]{5}/, "inválido"); // limita pra máximo 999.999.999,99
	v = v.replace(/(\d{1})(\d{1,1})$/, "$1.$2"); // coloca virgula antes dos
	// últimos 2 digitos
	z.value = v;
}

function superficieCorporal(z) {
	v = z.value;
	v = v.replace(/\D/g, ""); // permite digitar apenas números
	v = v.replace(/[0-9]{5}/, "inválido"); // limita pra máximo 999.999.999,99
	v = v.replace(/(\d{1})(\d{1,1})$/, "$1.$2"); // coloca virgula antes dos
	// últimos 2 digitos
	z.value = v;
}

function pesquisaServicos() {
	// Abre modalExcluir
	$('#pesquisaServicos').modal('toggle');
}

function modalPagamentoRetorno() {
	$('#pagamentoRetorno').modal('show');
}


function carregando(prm) {
	$('#' + prm).modal({
		keyboard : false
	});
}
function carregandoFim(prm) {
	$('#' + prm).modal('hide');
}
